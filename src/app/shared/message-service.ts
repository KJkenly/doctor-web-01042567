import { Injectable } from '@angular/core';
import { Message, MessageService } from 'primeng/api';


// @Injectable({
//     providedIn: 'root',
// })
export class MessageServices {   
    messages: any | undefined;
    constructor(
        private messageService: MessageService,


    ) {}
    // info  warn success error 

    showErrorViaToast(severity: any, summary: any, detail: any) {
        this.messageService.add({  severity: severity, summary:summary, detail: detail  });
    }

    showSuccessViaToast(severity: any, summary: any, detail: any) {
        this.messageService.add({  severity: severity, summary:summary, detail: detail });
    }



    showErrorViaMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }

    showSuccessViaMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }




}
