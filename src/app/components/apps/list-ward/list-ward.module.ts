import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { ListWardRoutingModule } from './list-ward-routing.module';
import { ListWardComponent } from './list-ward.component';
import { AvatarModule } from 'primeng/avatar';
import { DropdownModule } from 'primeng/dropdown';

import { DataViewModule } from 'primeng/dataview';
import { InputTextModule } from 'primeng/inputtext';

import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { NgxSpinnerModule } from "ngx-spinner";




@NgModule({
  declarations: [
    ListWardComponent
  ],
  imports: [
    CommonModule,
    ListWardRoutingModule,FormsModule,ReactiveFormsModule ,
    AvatarModule,DropdownModule,DataViewModule,ButtonModule,DividerModule,NgxSpinnerModule,InputTextModule
  ]
})
export class ListWardModule { }
