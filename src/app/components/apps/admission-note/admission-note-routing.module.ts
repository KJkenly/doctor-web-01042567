import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdmissionNoteComponent} from './admission-note.component';

const routes: Routes = [
  {
    path:'',component:AdmissionNoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmissionNoteRoutingModule { }
