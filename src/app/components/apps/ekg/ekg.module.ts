import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EkgRoutingModule } from './ekg-routing.module';
import { EkgComponent } from './ekg.component';
import {SharedModule} from '../../../shared/sharedModule';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ButtonModule } from 'primeng/button';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SpeedDialModule } from 'primeng/speeddial';

@NgModule({
  declarations: [
    EkgComponent
  ],
  imports: [
    CommonModule,SharedModule,
    CommonModule,
    EkgRoutingModule,
    MessageModule,
    MessagesModule,
    ButtonModule,
    NgxSpinnerModule,
    SpeedDialModule
  ]
})
export class EkgModule { }
