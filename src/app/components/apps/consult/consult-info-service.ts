import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultInfoService {

  pathPrefixLookup: any = `api-lookup/lookup`
  pathPrefixDoctor: any = `api-doctor/doctor`
  pathPrefixAuth: any = `api-auth/auth`

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  })

  private axiosLookup = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  })

  constructor() {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })




    this.axiosLookup.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosLookup.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }


  async getPatientInfo(admitId: any) {
    console.log(admitId);

    const url = `/admit/${admitId}/patient-info`
    console.log(url);

    return await this.axiosInstance.get(url)
  }

  async saveDoctorOrder(data: object) {
    return await this.axiosInstance.post('/doctor-order', data)
  }

  async removeOrders(OrderId: any) {
    const url = `/doctor-order/order/${OrderId}`
    return await this.axiosInstance.delete(url)
  }

  async getLookupMedicine() {
    // console.log('getLookupMedicine');
    const url = `/medicine`
    return await this.axiosLookup.get(url)
  }

  async getLookupItem() {
    const url = `/items/list`
    return await this.axiosLookup.get(url)
  }

  async saveProgressNote(data: object) {
    const url = `/progress-note`
    return await this.axiosInstance.post(url, data)
  }

  async getDoctorOrderById(admitId: any) {
    // console.log(admitId);

    const url = `/doctor-order/${admitId}/admit`

    return await this.axiosInstance.get(url)
  }
  
  async getLookupDoctor() {
    // console.log('getLookupDoctor');
    const url = `/doctor `
    return await this.axiosLookup.get(url)
  }

  async getLookupDoctorinfoByID(id: any) {
    // console.log('getLookupDoctorId');
    const url = `/doctor/infoByID/${id}`
    return await this.axiosLookup.get(url)
  }

}
