export const environment = {
  production: true,
  apiUrl: 'http://113.53.233.170',
  pathPrefixLookup: `api-lookup/lookup`,
  pathPrefixNurse:`api-doctor/doctor`,
  pathPrefixAuth: `api-auth/auth`
};
